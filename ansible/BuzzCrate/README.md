# BuzzCrate 

Project folder for provisioning BuzzCrate. 

## Inventory

Inventory for the cluster with variables of the project located in buzzcrate.hosts. 

## Bootstrap

bootstrap.yaml

## Deploy k3s

deploycluster.yaml

# Workflow... 
Really this is poor documentation with stuff I'm doing.

1. Comment out any passwords or users for Ansible in the hosts file. 
1. *basics.sh*: Run the script to get the base image updated, add python, allows Ansible to pick up form there. 
1. *setuser.yaml*: Playbook to call a rool to set user and ssh keys. You will need to comment out any current users and passwords from the hosts file. 
1. Uncomment out information from line 1. 
1. *prep.yaml*: Preps the system with other packages needed for running k3's, removes ALARM user account. 
1. `ansible-playbook k3_install_activate.yaml --tags "install,join" --skip-tags "disjoin,uninstall"`

**NOTE**: To use any roles, `ln -s ../roles roles` first. 
